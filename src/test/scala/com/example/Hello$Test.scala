package com.example

import org.scalatest.FunSuite

/**
  * Created by webstc1 on 04/08/16.
  */
class Hello$Test extends FunSuite {

  test("testGreet") {
    val p = "Bob"

    val expected = s"Sut mae, $p!"

    val result = Hello.greet(p)
    assert(result === expected)
  }

}
