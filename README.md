# Scala CI 101 (** public repo **)#

### What is this repository for? ###

* Minimal Scala application intended to allow me to explore CI with Scala and Jenkins.
* 1.0

### Jenkins server ###

* Set up a Jenkins server on Google Compute Platform using the Bitnami launcher.
* This sets up a Jenkins server with authentication enabled and various defaults.
* You can also install Jenkins easily on your own machine, but GCP is a nice option as it can be shared easily.

### SBT and Jenkins ###

* Tried installing SBT on Jenkins (Debian) server [using Aptitude](http://www.scala-sbt.org/0.13/docs/Installing-sbt-on-Linux.html), but Jenkins did not seem to like this.
* Also tried telling Jenkins to install SBT automatically, but this did not work either.
* Ended up installing SBT by [manually downloading .zip archive](http://www.scala-sbt.org/download.html), then setting up SBT and the [Jenkins SBT plugin](https://wiki.jenkins-ci.org/display/JENKINS/sbt+plugin) as described by [Whiteboard Coder](http://www.whiteboardcoder.com/2014/01/jenkins-and-sbt.html).
* Then you can create a Build Step using SBT that runs SBT from sbt-launch.jar as described above.

### Test results ###

* You can run `sbt test` as a (separate?) Build Step, and publish the test results to a specified folder.
* You need to make sure Jenkins has the [JUnit plugin](https://wiki.jenkins-ci.org/display/JENKINS/JUnit+Plugin) installed.
* Add a Post-build Action for "Publish JUnit test result report" and specify `target/test-reports/*.xml` as the folder for the Test report XMLs.
* When you run your build, Jenkins should be able to pick up the test results and display them in the browser UI.